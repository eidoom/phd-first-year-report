(* ::Package:: *)

(* ::Section:: *)
(*Plotting limits*)

labels={Subscript["y","3"],Subscript["y","4"]};
limitsPlotRaw=RegionPlot[y[3]+y[4]<=1,{y[3],0,1},{y[4],0,1}];
divergences=Plot[0,{y[3],0,1},
	Epilog->{
		{AbsoluteThickness[8],Red,Line[{{0,0},{0,1}}]},
		{AbsoluteThickness[8],Red,Line[{{0,0},{1,0}}]},
		{PointSize[0.1],Red,Point[{0,0}]}
		},
	AxesLabel->labels,
	PlotRange->{{0,1},{0,1}},
	AspectRatio->1
	];
limitsPlot=Show[{divergences,limitsPlotRaw}];
place=StringJoin[Directory[],"/limitsPlot.pdf"];
Export[place,limitsPlot];