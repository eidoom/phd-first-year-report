# [phd-first-year-report](https://gitlab.com/eidoom/phd-first-year-report)

## Dependencies

```shell
sudo dnf install texlive-collection-basic texlive-mathtools texlive-physics texlive-cleveref texlive-tikz-feynman texlive-luatex texlive-bibtex latexmk
```

## Build

```shell
./build.sh
```

## Online

[Live](https://eidoom.gitlab.io/phd-first-year-report/progress-report-first-year-phd.pdf)

## Tools

<!-- The `tex` files were autoindented with `latexindent -w -l latexindentSettings.yaml *.tex`. -->
<!-- Install with -->
<!-- ```shell -->
<!-- sudo dnf install texlive-latexindent -->
<!-- ``` -->

The `bib` file is formatted with `bibtool` using
```shell
bibtool -i progress-report-first-year-phd.bib -o progress-report-first-year-phd.bib -s -d -@
```
Install with
```shell
sudo dnf install BibTool
```
